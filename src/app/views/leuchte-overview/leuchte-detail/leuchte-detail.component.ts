import {Component, OnInit, ViewChild} from '@angular/core';
import {LeuchteOverviewService} from '../services/leuchte-overview.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Leuchte} from '../leuchte';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-leuchte-detail',
  templateUrl: './leuchte-detail.component.html',
  styleUrls: ['./leuchte-detail.component.css']
})
export class LeuchteDetailComponent implements OnInit {
  fileName = '';
  type = '';
  adresse = 0;
  isBetriebzeitDefect = '';
  isTreibertemperaturDefect = '';
  isSpannungDefect = '';
  isPowerfactorDefect = '';

  leuchtenByAdresse: Array<Leuchte>[] = [];

  leuchtenDetails: Array<Leuchte>[];
  displayedColumns: string[] = [ 'zeitstempel',
  'adresse',
  'anwesend',
  'lichtlevel',
  'betriebZei',
  'anzahlS',
  'exVerSP',
  'freqVersp',
  'leistungFA',
  'tempTrei',
  'prozStro',
  'anzahlST',
  'anzahlSR',
  'betriebZRu',
  'spanung',
  'strom',
  'tempLed',
  'wirkenergie',
  'wirkerSC',
  'wirkleistung',
  'wirkleisSk',
  'sheinenerg',
  'sheiESK',
  'sheinLeist',
  'sheinleisSk',
  'lastenerg',
  'lastenergSk',
  'undefined'];
  dataList: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private leuchteService: LeuchteOverviewService,
              private router: Router,
              private acRoute: ActivatedRoute
  ) {
    this.dataList = new MatTableDataSource(this.leuchtenDetails);
  }

  ngOnInit(): void {
    this.fileName = this.acRoute.snapshot.queryParams.fileName;
    this.type = this.acRoute.snapshot.queryParams.type;
    this.adresse = this.acRoute.snapshot.queryParams.adresse;
    this.isBetriebzeitDefect = this.acRoute.snapshot.queryParams.betriebzeit;
    this.isSpannungDefect = this.acRoute.snapshot.queryParams.spannung;
    this.isTreibertemperaturDefect = this.acRoute.snapshot.queryParams.treibertemperatur;
    this.isPowerfactorDefect = this.acRoute.snapshot.queryParams.powerfactor;
    this.getLeuchteDetail(this.fileName, this.type);
  }

  getLeuchteDetail(fileName, type) {
    this.leuchteService.getDataByJsonName(fileName, type).subscribe(
      data => {
        this.leuchtenDetails = data;
        for (let i = 0; i < this.leuchtenDetails.length; i++) {
          // @ts-ignore
          if (this.leuchtenDetails[i].adresse === this.adresse) {
            this.leuchtenByAdresse.push(this.leuchtenDetails[i]);
          }
        }
        this.leuchtenDetails = this.leuchtenByAdresse;
        this.dataList = new MatTableDataSource(this.leuchtenDetails);
        this.dataList.paginator = this.paginator;
        this.dataList.sort = this.sort;
      }
    );
  }

  ngAfterViewInit() {
    this.dataList.paginator = this.paginator;
    this.dataList.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataList.filter = filterValue.trim().toLowerCase();

    if (this.dataList.paginator) {
      this.dataList.paginator.firstPage();
    }
  }

  showStat() {
    this.router.navigate(['../statistik/'], { queryParams: {fileName: this.fileName, adresse: this.adresse}, relativeTo: this.acRoute });
  }
  showZeit() {
    this.router.navigate(['../betriebzeit/'], { queryParams: {fileName: this.fileName, adresse: this.adresse}, relativeTo: this.acRoute });
  }
  showTemperatur() {this.router.navigate(['../treibertemperature/'], { queryParams: {fileName: this.fileName, adresse: this.adresse}, relativeTo: this.acRoute });
  }
  showPowerfactor() {this.router.navigate(['../powerfactor/'], { queryParams: {fileName: this.fileName, adresse: this.adresse}, relativeTo: this.acRoute });
  }
}

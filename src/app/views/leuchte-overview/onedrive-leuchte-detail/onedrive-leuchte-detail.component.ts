import { Component, OnInit } from '@angular/core';
import {LeuchteOverviewService} from '../services/leuchte-overview.service';

@Component({
  selector: 'app-onedrive-leuchte-detail',
  templateUrl: './onedrive-leuchte-detail.component.html',
  styleUrls: ['./onedrive-leuchte-detail.component.css']
})
export class OnedriveLeuchteDetailComponent implements OnInit {

  constructor(private leuchteService: LeuchteOverviewService) { }

  ngOnInit(): void {
    this.downloadOnedriveData();

    this.getOnedriveData();
  }
  getOnedriveData() {
    this.leuchteService.getDataFromOnDrive().subscribe(
      data => {
        console.log('####Ondeirve: ' + data);
      },
      error => {
        console.error(error);
      }
    );
  }

  downloadOnedriveData() {
    this.leuchteService.downloadDataFromOnDrive().subscribe(
      data => {
        console.log('####Download: ' + data);
      },
      error => {
        console.error(error);
      }
    );
  }

}

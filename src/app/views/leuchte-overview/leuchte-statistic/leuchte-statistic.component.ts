import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {LeuchteOverviewService} from '../services/leuchte-overview.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Leuchte} from '../leuchte';

@Component({
  selector: 'app-leuchte-statistic',
  templateUrl: './leuchte-statistic.component.html',
  styleUrls: ['./leuchte-statistic.component.css']
})
export class LeuchteStatisticComponent implements OnInit {
  fileName = '';
  adresse = 0;
  leuchtenByAdresse: Array<Leuchte>[] = [];
  leuchtenZeitStempel: Array<string>[] = [];
  leuchtenSpanung: Array<number>[] = [];
  leuchtenDetails: Array<Leuchte>[];
  lineChartLegend = true;
  lineChartType = 'line';

  lineChartData: Array<any> = [
    {data: this.leuchtenSpanung, label: 'Series A'},
  ];

  public lineChartLabels: Array<any> = this.leuchtenZeitStempel;
  public lineChartOptions: any = {
    animation: false,
    responsive: true,
  };

  public lineChartColours: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  ngOnInit(): void {
    this.fileName = this.acRoute.snapshot.queryParams.fileName;
    this.adresse = this.acRoute.snapshot.queryParams.adresse;
    this.getLeuchteDetail();
  }



  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private leuchteService: LeuchteOverviewService,
              private router: Router,
              private acRoute: ActivatedRoute) { }


  getLeuchteDetail() {
    this.leuchteService.getBonusDataByJsonName(this.fileName).subscribe(
      data => {
        this.leuchtenDetails = data;
        for (let pointer = 0; pointer < this.leuchtenDetails.length; pointer++) {
          // @ts-ignore
          if (this.leuchtenDetails[pointer].adresse === this.adresse) {
            this.leuchtenByAdresse.push(this.leuchtenDetails[pointer]);
          }
        }
        for (let i = 0; i < 13; i++) {
          // @ts-ignore
          this.leuchtenZeitStempel.push(this.leuchtenByAdresse[i].zeitstempel.split(' ')[1]);
          // @ts-ignore
          this.leuchtenSpanung.push(this.leuchtenByAdresse[i].spanung);
        }
      }
    );
  }
}

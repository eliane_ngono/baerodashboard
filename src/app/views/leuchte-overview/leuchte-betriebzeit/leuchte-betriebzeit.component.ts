import { Component, OnInit } from '@angular/core';
import {Leuchte} from '../leuchte';
import {LeuchteOverviewService} from '../services/leuchte-overview.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-leuchte-betriebzeit',
  templateUrl: './leuchte-betriebzeit.component.html',
  styleUrls: ['./leuchte-betriebzeit.component.css']
})
export class LeuchteBetriebzeitComponent implements OnInit {
  fileName = '';
  adresse = 0;
  leuchtenByAdresse: Array<Leuchte>[] = [];
  leuchtenZeitStempel: Array<string>[] = [];
  leuchtenBeztriebzeit: Array<number>[] = [];
  leuchtenDetails: Array<Leuchte>[];
  lineChartLegend = true;
  lineChartType = 'line';

  lineChartData: Array<any> = [
    {data: this.leuchtenBeztriebzeit, label: 'Series A'},
  ];

  public lineChartLabels: Array<any> = this.leuchtenZeitStempel;
  public lineChartOptions: any = {
    animation: false,
    responsive: true,
  };

  public lineChartColours: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  ngOnInit(): void {
    this.fileName = this.acRoute.snapshot.queryParams.fileName;
    this.adresse = this.acRoute.snapshot.queryParams.adresse;
    this.getLeuchteDetail();
  }



  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private leuchteService: LeuchteOverviewService,
              private router: Router,
              private acRoute: ActivatedRoute) { }


  getLeuchteDetail() {
    this.leuchteService.getBonusDataByJsonName(this.fileName).subscribe(
      data => {
        this.leuchtenDetails = data;
        for (let pointer = 0; pointer < this.leuchtenDetails.length; pointer++) {
          // @ts-ignore
          if (this.leuchtenDetails[pointer].adresse === this.adresse) {
            this.leuchtenByAdresse.push(this.leuchtenDetails[pointer]);
          }
        }
        for (let j = 0; j < 13; j++) {
          // @ts-ignore
          this.leuchtenZeitStempel.push(this.leuchtenByAdresse[j].zeitstempel.split(' ')[1]);
          // @ts-ignore
          this.leuchtenBeztriebzeit.push(this.leuchtenByAdresse[j].betriebZei);
          // @ts-ignore
          if (this.leuchtenByAdresse[j].betriebZei ===  '3950.5') {
            // @ts-ignore
            this.alarmDefectPosibility();
          }
        }
      }
    );
  }

  alarmDefectPosibility() {
    // TODO
  }
}


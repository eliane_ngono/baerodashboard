import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LeuchteBonusComponent} from './leuchte-bonus/leuchte-bonus.component';
import {MsalGuard} from '@azure/msal-angular';
import {LeuchteDetailComponent} from './leuchte-detail/leuchte-detail.component';
import {OnedriveLeuchteDetailComponent} from './onedrive-leuchte-detail/onedrive-leuchte-detail.component';
import {LeuchteStatisticComponent} from './leuchte-statistic/leuchte-statistic.component';
import {LeuchteAdresseOverviewComponent} from './leuchte-adresse-overview/leuchte-adresse-overview.component';
import {LeuchteBetriebzeitComponent} from './leuchte-betriebzeit/leuchte-betriebzeit.component';
import {LeuchteTreiberTemperatureComponent} from './leuchte-treiber-temperature/leuchte-treiber-temperature.component';
import {LeuchtePowerfactorComponent} from './leuchte-powerfactor/leuchte-powerfactor.component';
import {LeuchteBaeroComponent} from './leuchte-baero/leuchte-baero.component';

const routes: Routes = [
  {
    canActivate: [
      MsalGuard
    ],
    path: '',
    data: {
      title: 'Leuchte Overview'
    },
    children: [
      {
        path: '',
        redirectTo: 'bonus'
      },
      {
        path: 'bonus',
        component: LeuchteBonusComponent,
        data: {
          title: 'bonus'
        }
      },
      {
        path: 'baero',
        component: LeuchteBaeroComponent,
        data: {
          title: 'baero'
        }
      },
      {
        path: 'detail',
        component: LeuchteDetailComponent,
        data: {
          title: 'detail'
        }
      },
      {
        path: 'onedrive-detail',
        component: OnedriveLeuchteDetailComponent,
        data: {
          title: 'onedrive-detail'
        }
      },
      {
        path: 'adresse',
        component: LeuchteAdresseOverviewComponent,
        data: {
          title: 'adresse'
        }
      },
      {
        path: 'statistik',
        component: LeuchteStatisticComponent,
        data: {
          title: 'Statistik'
        }
      },
      {
        path: 'betriebzeit',
        component: LeuchteBetriebzeitComponent,
        data: {
          title: 'Betriebzeit'
      }
     },
      {
        path: 'treibertemperature',
        component: LeuchteTreiberTemperatureComponent,
        data: {
          title: 'Treibertemperature'
        }
      },
      {
        path: 'powerfactor',
        component: LeuchtePowerfactorComponent,
        data: {
          title: 'Powerfactor'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeuchteOverviewRoutingModule {}

export class Leuchte {

  zeitstempel: string;
  adresse: string;
  anwesend: string;
  lichtlevel: string;
  BetriebZei: string;
  anzahlS: string;
  exVerSP: string;
  freqVersp: string;
  leistungFA: string;
  tempTrei: string;
  prozStro: string;
  anzahlST: string;
  anzahlSR: string;
  betriebZRu: string;
  spanung: string;
  strom: string;
  tempLed: string;
  wirkenergie: string;
  wirkerSC: string;
  wirkleistung: string;
  wirkleisSk: string;
  sheinenerg: string;
  SheiESK: string;
  sheinLeist: string;
  sheinleisSk: string;
  lastenerg: string;
  lastenergSk: string;
  undefined: string;
}


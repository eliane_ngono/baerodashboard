import {Component, OnInit, ViewChild} from '@angular/core';
import {Leuchte} from '../leuchte';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {LeuchteOverviewService} from '../services/leuchte-overview.service';
import {LeuchteJson} from '../leuchteJson';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-leuchte-all',
  templateUrl: './leuchte-bonus.component.html',
  styleUrls: ['./leuchte-bonus.component.css']
})
export class LeuchteBonusComponent implements OnInit {


  selectedLeuchte: Leuchte;
  leuchten: Leuchte[];
  leuchtenJsonNames: Array<LeuchteJson> = [];

  displayedColumns: string[] = ['Name', 'Action'];
  listJsonName: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private leuchteService: LeuchteOverviewService,
              private router: Router,
              private acRoute: ActivatedRoute,
  ) {
    this.listJsonName = new MatTableDataSource(this.leuchtenJsonNames);
  }
  ngAfterViewInit() {
    this.listJsonName.paginator = this.paginator;
    this.listJsonName.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.listJsonName.filter = filterValue.trim().toLowerCase();

    if (this.listJsonName.paginator) {
      this.listJsonName.paginator.firstPage();
    }
  }
  ngOnInit(): void {
    this.getJsonNames();
  }
onSelect(leuchte: Leuchte): void {

    this.selectedLeuchte = leuchte;
  }
  getJsonNames() {
   this.leuchteService.getJsonNameList().subscribe(
     data => {
       const arrayText = data.split('\n');
       for (let i = 0; i < arrayText.length; i++) {
         const leuchJsonObj = new LeuchteJson();
         leuchJsonObj.name = arrayText[i];
         this.leuchtenJsonNames.push(leuchJsonObj);
       }
       this.listJsonName = new MatTableDataSource(this.leuchtenJsonNames);
       this.listJsonName.paginator = this.paginator;
       this.listJsonName.sort = this.sort;
     },
     error => {
       console.error(error);
     }
   );
  }

  showData(element: any) {
    this.router.navigate(['../adresse/'], { queryParams: {fileName: element.name, type: 'bonus'}, relativeTo: this.acRoute });
  }

  goToOndrive() {
    this.router.navigate(['../onedrive-detail/'], { relativeTo: this.acRoute });
  }
}

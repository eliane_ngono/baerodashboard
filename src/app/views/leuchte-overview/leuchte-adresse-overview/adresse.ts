export class Adresse {
  adresseNumber: number;
  isDefect: boolean;
  isBetriebzeitDefect: boolean;
  isSpannungDefect: boolean;
  isTreibertemperaturDefect: boolean;
  isPowerfactorDefect: boolean;

}

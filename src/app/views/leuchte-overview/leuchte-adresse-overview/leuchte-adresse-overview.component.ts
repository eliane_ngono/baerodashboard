import { Component, OnInit } from '@angular/core';
import {LeuchteOverviewService} from '../services/leuchte-overview.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Leuchte} from '../leuchte';
import {Adresse} from './adresse';

@Component({
  selector: 'app-leuchte-adresse-overview',
  templateUrl: './leuchte-adresse-overview.component.html',
  styleUrls: ['./leuchte-adresse-overview.component.css']
})


export class LeuchteAdresseOverviewComponent implements OnInit {
  fileName = '';
  type = '';
  adresse = 0;
  adresseList = [];
  adressListNumber = [];
  leuchten: Array<Leuchte>[];
  isAdresseDefect = false;
  isAdrDefect = false;

  constructor(private leuchteService: LeuchteOverviewService,
              private router: Router,
              private acRoute: ActivatedRoute
  ) {}


  ngOnInit(): void {
    this.fileName = this.acRoute.snapshot.queryParams.fileName;
    this.type = this.acRoute.snapshot.queryParams.type;
    this.getAdresseByJson();
  }

  getAdresseByJson() {
    this.leuchteService.getDataByJsonName(this.fileName, this.type).subscribe(
      data => {
        this.leuchten = data;
        for (let i = 0; i < this.leuchten.length; i++) {
          const currentAdresse = new Adresse();
          // @ts-ignore
          if (!this.adressListNumber.includes(this.leuchten[i].adresse)) {
            // @ts-ignore
            this.adressListNumber.push(this.leuchten[i].adresse);
            // @ts-ignore
            currentAdresse.adresseNumber = this.leuchten[i].adresse;
            // @ts-ignore
            this.isAdrDefect = this.isDefectCheker(currentAdresse);
            // @ts-ignore
            this.adresseList.push(currentAdresse);
            this.isAdresseDefect = false;
          }
        }
      },
      error => {
        console.error(error);
      }
    );
  }

   isDefectCheker(currentAdresse: Adresse) {
    for (let j = 0; j < this.leuchten.length; j++) {
      // @ts-ignore
      if (this.leuchten[j].adresse === currentAdresse.adresseNumber) {

        currentAdresse.isDefect = false;
        currentAdresse.isSpannungDefect = false;
        currentAdresse.isBetriebzeitDefect = false;
        currentAdresse.isTreibertemperaturDefect = false;
        currentAdresse.isPowerfactorDefect = false;

        // @ts-ignore
        if (Number(this.leuchten[j].betriebZei) > 5000) {
          currentAdresse.isBetriebzeitDefect = true;
          currentAdresse.isDefect = true;
          // @ts-ignore
        }
        // @ts-ignore
        if (Number(this.leuchten[j].spanung) > 34.7 ) {
          currentAdresse.isDefect = true;
          currentAdresse.isSpannungDefect = true;
        }
        // @ts-ignore
        if (Number(this.leuchten[j].tempTrei ) >  70 ) {
          currentAdresse.isDefect = true;
          currentAdresse.isTreibertemperaturDefect = true;
        }
        // @ts-ignore
        if (Number(this.leuchten[j].leistungFA) > 1 ) {
          currentAdresse.isDefect = true;
          currentAdresse.isPowerfactorDefect = true;
        }

      }
    }
    return this.isAdresseDefect;
  }

  showDetails(adresse: Adresse) {
    this.router.navigate(['../detail/'],
      { queryParams: {
          fileName: this.fileName,
          adresse: adresse.adresseNumber,
          betriebzeit: adresse.isBetriebzeitDefect,
          spannung: adresse.isSpannungDefect,
          treibertemperatur: adresse.isTreibertemperaturDefect,
          powerfactor: adresse.isPowerfactorDefect,

        },
        relativeTo: this.acRoute });
  }
}

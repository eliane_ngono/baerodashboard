import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeuchteBonusComponent } from './leuchte-bonus/leuchte-bonus.component';
import {MatButtonModule} from '@angular/material/button';


// Theme Routing
import { LeuchteOverviewRoutingModule } from './leuchte-overview-routing.module';
import {FormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from '@angular/material/icon';
import { LeuchteDetailComponent } from './leuchte-detail/leuchte-detail.component';
import { OnedriveLeuchteDetailComponent } from './onedrive-leuchte-detail/onedrive-leuchte-detail.component';
import { LeuchteStatisticComponent } from './leuchte-statistic/leuchte-statistic.component';
import {ChartsModule} from 'ng2-charts';
import {LeuchteAdresseOverviewComponent} from "./leuchte-adresse-overview/leuchte-adresse-overview.component";
import { LeuchteBetriebzeitComponent } from './leuchte-betriebzeit/leuchte-betriebzeit.component';
import {MatDividerModule} from '@angular/material/divider';
import { LeuchteTreiberTemperatureComponent } from './leuchte-treiber-temperature/leuchte-treiber-temperature.component';
import { LeuchtePowerfactorComponent } from './leuchte-powerfactor/leuchte-powerfactor.component';
import { LeuchteBaeroComponent } from './leuchte-baero/leuchte-baero.component';


@NgModule({
  declarations: [
    LeuchteBonusComponent,
    LeuchteDetailComponent,
    OnedriveLeuchteDetailComponent,
    LeuchteAdresseOverviewComponent,
    LeuchteStatisticComponent,
    LeuchteBetriebzeitComponent,
    LeuchteTreiberTemperatureComponent,
    LeuchtePowerfactorComponent,
    LeuchteBaeroComponent
  ],
  imports: [
    CommonModule,
    LeuchteOverviewRoutingModule,
    FormsModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatInputModule,
    MatIconModule,
    ChartsModule,
    MatButtonModule,
    MatDividerModule
  ]
})
export class LeuchteOverviewModule { }

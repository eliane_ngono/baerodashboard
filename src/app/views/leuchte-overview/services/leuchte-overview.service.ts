import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';


const testFolder = '../../assets/leuchteData/';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class LeuchteOverviewService {

  ondrive_url = 'https://graph.microsoft.com/v1.0/me/drive/items/F97C070A0D6B1644!5891/content?format=json';
  ondrive_url2 = 'https://graph.microsoft.com/v1.0/me/drive/items/F97C070A0D6B1644!5891/content';
  url_local_json_bonus = '../../assets/leuchteBonusData/leuchtenBonusJsonName.txt';
  url_local_json_baero = '../../assets/leuchteBaeroData/leuchtenBaeroJsonName.txt';
  base_url_bonus = '../../assets/leuchteBonusData/';
  base_url_baero = '../../assets/leuchteBaeroData/';

  constructor(private http: HttpClient) { }
  urlToUse = '';
  getJsonNameList(): Observable<any> {
    return this.http.get(this.url_local_json_bonus, { responseType: 'text' });
  }
  getBonusDataByJsonName(currentUrl): Observable<any> {
    return this.http.get(this.base_url_bonus + currentUrl, httpOptions);
  }
  getBaeroJsonNameList(): Observable<any> {
    return this.http.get(this.url_local_json_baero, { responseType: 'text' });
  }

  getDataByJsonName(currentUrl, type): Observable<any> {
    if (type === 'bonus') {
      this.urlToUse = this.base_url_bonus;
    }
    if (type === 'baero') {
      this.urlToUse = this.base_url_baero;
    }
    return this.http.get(this.urlToUse + currentUrl, httpOptions);
  }
  getDataFromOnDrive(): Observable<any> {
    return this.http.get(this.ondrive_url, httpOptions);
  }
  downloadDataFromOnDrive(): Observable<any> {
    return this.http.get(this.ondrive_url2, httpOptions);
  }
}

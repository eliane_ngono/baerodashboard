import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    title: true,
    name: 'Leuchte Overview'
  },
  {
    name: 'Leuchten Bonus',
    url: '/leuchte-overview/bonus',
    icon: 'icon-bulb'
  },
  {
    name : 'Leuchten Bäro',
    url: '/leuchte-overview/baero',
    icon: 'icon-bulb'

  },
  {
    name : 'Einstellung',
    url: '/test',
    icon: 'icon-settings'

  },
  //
  // {
  //   title: true,
  //   name: 'Components'
  // },
  // {
  //   name: 'Base',
  //   url: '/base',
  //   icon: 'icon-puzzle',
  //   children: [
  //
  //     {
  //       name: 'Carousels',
  //       url: '/base/carousels',
  //       icon: 'icon-puzzle'
  //     },
  //
  //   ]
  // },




  {
    divider: true
  },




];
